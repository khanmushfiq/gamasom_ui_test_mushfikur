import React from "react";
import { Switch, Route, withRouter } from "react-router-dom";
import UserList from "./user_list";
import UserDetails from "./user-details";
import "./App.css";

function App() {
  return (
    <div className="user-list">
      <Switch>
        <Route exact path="/" component={UserList} />
      </Switch>
      <Switch>
        <Route exact path="/create-details/:user" component={UserDetails} />
      </Switch>
    </div>
  );
}

export default withRouter(App);
