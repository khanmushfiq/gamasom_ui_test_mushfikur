import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import "./index.css";
import App from "./App";

const rootEl = document.getElementById("root");

const render = AppComponent => {
  let view = (
    <Router>
      <AppComponent />
    </Router>
  );

  ReactDOM.render(view, rootEl);
};
render(App);
