import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import Tooltip from "@material-ui/core/Tooltip";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Dialog from "@material-ui/core/Dialog";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Avatar from "@material-ui/core/Avatar";
import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import SearchIcon from "@material-ui/icons/Search";
import axios from "axios";
import List from "@material-ui/core/List";
import InputBase from "@material-ui/core/InputBase";
import { lighten } from "@material-ui/core/styles/colorManipulator";
import { fade } from "@material-ui/core/styles/colorManipulator";
import { ListItem } from "@material-ui/core";

let counter = 0;
let newData = [];
let originalData = [];
function createData(name, calories, fat, carbs, protein) {
  counter += 1;
  return { id: counter, name, calories, fat, carbs, protein };
}

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === "desc"
    ? (a, b) => desc(a, b, orderBy)
    : (a, b) => -desc(a, b, orderBy);
}

const rows = [
  {
    id: "name",
    numeric: false,
    label: "Name"
  },
  {
    id: "image",
    numeric: true,
    label: "Image"
  },
  {
    id: "repos",
    numeric: true,
    label: "No of repos"
  },

  { id: "repo-list", numeric: true, disablePadding: false, label: "Repos list" }
];

class EnhancedTableHead extends React.Component {
  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };

  render() {
    const {
      onSelectAllClick,
      order,
      orderBy,
      numSelected,
      rowCount
    } = this.props;

    return (
      <TableHead>
        <TableRow>
          {rows.map(
            row => (
              <TableCell
                key={row.id}
                align={row.numeric ? "right" : "left"}
                padding={row.disablePadding ? "none" : "default"}
                sortDirection={orderBy === row.id ? order : false}
              >
                <Tooltip
                  title="Sort"
                  placement={row.numeric ? "bottom-end" : "bottom-start"}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={orderBy === row.id}
                    direction={order}
                    onClick={this.createSortHandler(row.id)}
                  >
                    {row.label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            ),
            this
          )}
        </TableRow>
      </TableHead>
    );
  }
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired
};

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit
  },
  highlight:
    theme.palette.type === "light"
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85)
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark
        },
  spacer: {
    flex: "1 1 100%"
  },
  actions: {
    color: theme.palette.text.secondary
  },
  title: {
    flex: "0 0 auto"
  },
  grow: {
    flexGrow: 1
  },

  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.black, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.black, 0.25)
    },
    marginLeft: 0,
    // width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing.unit,
      width: "auto"
    },
    color: "#111"
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  inputRoot: {
    color: "inherit",
    width: "100%"
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: 107,
      // height: 23,
      "&:focus": {
        width: 200
      }
    }
  },
  margin: {
    margin: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 3
  }
});

let EnhancedTableToolbar = props => {
  const { numSelected, classes } = props;

  return (
    <Toolbar
      className={classNames(classes.root, {
        [classes.highlight]: numSelected > 0
      })}
    >
      <div className={classes.title}>
        {numSelected > 0 ? (
          <Typography color="inherit" variant="subtitle1">
            {numSelected} selected
          </Typography>
        ) : (
          <Typography variant="h6" id="tableTitle">
            User List
          </Typography>
        )}
      </div>
      <div className={classes.spacer} />
      <div style={{ marginRight: "5px" }}>
        <div className={classes.grow} />
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            placeholder="Search"
            onChange={props.search}
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput
            }}
          />
        </div>
      </div>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const styles = theme => ({
  card: {
    maxWidth: 500,
    marginLeft: "30%",
    marginTop: "2"
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  actions: {
    display: "flex"
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    backgroundColor: "red"
  }
});

class EnhancedTable extends React.Component {
  state = {
    open: true,
    fullWidth: true,
    maxWidth: "sm",
    loading: false,
    order: "asc",
    orderBy: "email",
    selected: [],
    data: [],
    page: 0,
    rowsPerPage: 5
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = "desc";

    if (this.state.orderBy === property && this.state.order === "desc") {
      order = "asc";
    }

    this.setState({ order, orderBy });
  };

  handleSelectAllClick = event => {
    if (event.target.checked) {
      this.setState(state => ({ selected: state.data.map(n => n.id) }));
      return;
    }
    this.setState({ selected: [] });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  componentDidMount() {
    this.getAllProviders();
  }
  getAllProviders = async () => {
    let result = [],
      length;
    let url = window.location.href;
    let reqBody = {
      username: url.substr(url.lastIndexOf("/") + 1)
    };
    console.log(reqBody);
    await axios
      .post(`http://192.168.2.98:8000/gamasome/user-details`, reqBody)
      .then(r => {
        console.log("Result--->", r);
        length = r.data.data.length;
        if (r.data.success) {
          r.data.data.map(d => {
            result.push({
              id: d.id,
              name: d.owner.login,
              repos: length,
              avatar_url: d.owner.avatar_url,
              repo_url: d.git_url
            });
          });
        }
        originalData = result;
        this.setState({ data: result });
      });
  };
  handleLoader = (load, value) => {
    this.setState({ loading: true });
    if (value) {
      newData =
        // this.state.data.length > 0 &&
        originalData.filter(r => {
          return (
            r.name.toLowerCase().includes(value.toLowerCase()) ||
            r.status.toLowerCase().includes(value.toLowerCase()) ||
            r.email.toLowerCase().includes(value.toLowerCase())
          );
        });
    } else {
      newData = originalData;
    }

    this.setState({ loading: false, data: newData });
  };

  onSearchClicked = e => {
    console.log(e.target.value);
    let value = e.target.value;
    //let filterArr = [...this.state.holidayList];
    //console.log(filterArr);
    // let searchVal = e.target.value;
    // handleLoader(true, e.target.value);
    // setTimeout(() => {
    // 	handleLoader(false, searchVal);
    // }, 3000);
    value = value.trim();
    if (value) {
      newData = originalData.filter(r => {
        return (
          r.name.toLowerCase().includes(value.toLowerCase()) ||
          r.email.toLowerCase().includes(value.toLowerCase()) ||
          r.phone.toLowerCase().includes(value.toLowerCase()) ||
          r.website.toLowerCase().includes(value.toLowerCase())
        );
      });
    } else {
      newData = originalData;
    }

    this.setState({ data: newData });
  };

  viewUser = () => {
    this.setState({
      open: true
    });
  };

  handleClose = () => {
    this.setState({
      open: false
    });
  };
  goBack = () => {
    this.props.history.push("/");
    this.setState({ anchorEl: null });
  };

  render() {
    const { classes } = this.props;
    const { data, order, orderBy, selected, rowsPerPage, page } = this.state;
    const emptyRows =
      rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);
    console.log(data && data.length > 0 && data[0].avatar_url);

    return (
      <Paper className={classes.root}>
        <EnhancedTableToolbar
          search={this.onSearchClicked}
          numSelected={selected.length}
        />

        <Dialog
          fullScreen
          open={this.state.open}
          onClose={this.handleClose}
          // TransitionComponent={Transition}
        >
          <AppBar className={classes.appBar}>
            <Toolbar>
              <IconButton
                color="inherit"
                onClick={this.goBack}
                aria-label="Close"
              >
                <CloseIcon />
              </IconButton>
            </Toolbar>
          </AppBar>

          <Card
            className={classes.card}
            style={{ marginTop: "5em", marginBottom: "3em" }}
          >
            <CardHeader
              avatar={
                <Avatar aria-label="Recipe" className={classes.avatar}>
                  {data && data.length > 0 ? data[0].repos : ""}
                </Avatar>
              }
              title={data && data.length > 0 ? data[0].name.toUpperCase() : ""}
            />
            <CardMedia
              className={classes.media}
              image={data && data.length > 0 ? data[0].avatar_url : ""}
              title={data && data.length > 0 ? data[0].name.toUpperCase() : ""}
            />
            <CardContent>
              <div
                style={{ overflowY: "scroll", height: "200px", padding: "1em" }}
              >
                <h4 style={{ marginLeft: "1em", color: "grey" }}>
                  List of repo urls
                </h4>
                <List>
                  {data &&
                    data.length > 0 &&
                    data.map((d, k) => {
                      return <ListItem key={k}>{d.repo_url}</ListItem>;
                    })}
                </List>
              </div>
            </CardContent>
          </Card>
        </Dialog>
      </Paper>
    );
  }
}

EnhancedTable.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withRouter(withStyles(styles)(EnhancedTable));
